﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Arkos.Data;

namespace Arkos.Workshop.Infrastructure.Domain
{
    public interface IFuelingsRepository
    {
        Task<ICollection<Fueling>> GetAll(bool includeAll = true);

        Task<Fueling> FindById(int id, bool includeAll = true);

        Task<IList<Fueling>> FindByCarId(int carId);

        Task<IList<Fueling>> FindByDriverId(int driverId);

        Task<Fueling> Create(int carId, int driverId, Fueling item);

        Task<Fueling> Update(int id, int carId, int driverId, Fueling item);

        Task<Fueling> Delete(int id);
    }
}