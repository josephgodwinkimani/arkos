﻿using System;
namespace Arkos.Data
{
    public class Fueling
    {
        public int Id { get; set; }

        public Car Car { get; set; }

        public Driver Driver { get; set; }

        public double Fuel { get; set; }

        public double Cost { get; set; }

        public int RefuelingMileage { get; set; }

        public DateTime? Date { get; set; }
    }
}
