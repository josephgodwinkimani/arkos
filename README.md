
# CRM Application for transport company as UI project

## Table of contets
* [About the project](#about-the-project)
* [Built with](#built-with)
* [Author](#author)

## About the project
Application has been written on request local transport company to helping managing cost keeping, calculate stats and showing in user friendly interface.

The project includes:
- API with EF
- UI
- Authorization


## Built with
<li>.NET 5
<ul>
<li><a href="#packages">Blazor</a></li>
<li><a href="#packages">Entity Framework</a></li>
<li><a href="https://mudblazor.com/">MudBlazor</a></li>
<li><a href="https://www.matblazor.com/">MatBlazor</a></li></li>


## Author
* [Michał Staruch](https://gitlab.com/krisrox)
