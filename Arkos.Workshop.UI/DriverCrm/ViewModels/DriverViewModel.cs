﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Arkos.Data;

namespace Arkos.Workshop.UI.DriverCrm.ViewModels
{
    public class DriverViewModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public bool IsActive { get; set; } = true;

        public List<Fueling> Fuelings { get; set; } 
    }
}
