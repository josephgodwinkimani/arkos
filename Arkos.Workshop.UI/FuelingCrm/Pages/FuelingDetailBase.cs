﻿
using System.Threading.Tasks;
using Arkos.Workshop.UI.CarCrm.Services.Domain;
using Arkos.Workshop.UI.CarCrm.ViewModels;
using Arkos.Workshop.UI.DriverCrm.Services.Domain;
using Arkos.Workshop.UI.DriverCrm.ViewModels;
using Microsoft.AspNetCore.Components;
using Arkos.Workshop.UI.FuelingCrm.Services.Domain;
using Arkos.Workshop.UI.FuelingCrm.ViewModels;

namespace Arkos.Workshop.UI.FuelingCrm.Pages
{
    

    public class FuelingDetailBase : ComponentBase
    {
        [Inject]
        public IFuelingService FuelingService { get; set; }

        [Inject]
        public ICarService CarService { get; set; }

        [Inject]
        public IDriverService DriverService { get; set; }

        [Parameter]
        public int FuelingId { get; set; }
        public FuelingViewModel Fueling { get; set; } = new FuelingViewModel();

        [Parameter]
        public int CarId { get; set; }
        public CarViewModel Car { get; set; }

        [Parameter]
        public int DriverId { get; set; }
        public DriverViewModel Driver { get; set; } 


        protected override async Task OnInitializedAsync()
        {
            Fueling = await FuelingService.GetById(FuelingId);
            Car = await CarService.GetById(CarId);
            Driver = await DriverService.GetById(DriverId);
        }
    }
}
