﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.FuelingCrm.Services.Domain;
using Arkos.Workshop.UI.CarCrm.Services.Domain;
using Arkos.Workshop.UI.CarCrm.ViewModels;
using Arkos.Workshop.UI.FuelingCrm.ViewModels;
using Arkos.Workshop.UI.DriverCrm.Services.Domain;
using Arkos.Workshop.UI.DriverCrm.ViewModels;
using MatBlazor;
using MudBlazor;
using System.Linq;
using System;

namespace Arkos.Workshop.UI.FuelingCrm.Pages
{
    public class FuelingBase : ComponentBase
    {
        [Inject]
        public IFuelingService FuelingService { get; set; }

        [Inject]
        public ICarService CarService { get; set; }

        [Inject]
        public IDriverService DriverService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        protected IMatToaster Toaster { get; set; }

        [Parameter]
        public string FuelingId { get; set; }

        public FuelingViewModel Fueling { get; set; } = new FuelingViewModel();

        public ICollection<FuelingViewModel> Fuelings { get; set; }

        public IList<CarViewModel> Cars { get; set; }

        public IList<DriverViewModel> Drivers { get; set; }

        protected readonly string[] FilterOperation = { "==", ">=", "<=" };

        protected readonly string[] FilterNumberTypes = { "Cost", "Fuel", "RefuelingMileage" };

        protected string SearchStringToFilterDialog { get; set; }

        protected string ChooseOperationToFilter { get; set; }

        protected string ChooseColumnToFilter { get; set; }

        protected CarViewModel CarId { get; set; }

        protected DriverViewModel DriverId { get; set; }

        protected double UpperValue { get; set; } = 0.0;

        protected double LowerValue { get; set; } = 0.0;

        protected DateTime? StartFilterDate { get; set; } = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

        protected DateTime? EndFilterDate { get; set; } = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

        protected bool DialogIsOpen;

        protected bool FilterDialogIsOpen;

        private bool _isEdit;

        protected bool Saved;

        protected bool SuccessValidation;

        protected override async Task OnInitializedAsync()
        {
            Fuelings = await FuelingService.GetAll(true);
        }

        protected async Task OpenDialog(bool isEdit, int fuelingId = 0)
        {
            Cars = await CarService.GetAll(true);
            Drivers = await DriverService.GetAll(true);

            Saved = false;
            _isEdit = isEdit;
            if (!isEdit)
            {
                Fueling = new FuelingViewModel { Id = 0 };
            }
            else
            {
                Fueling = await FuelingService.GetById(fuelingId);
            }

            DialogIsOpen = true;
        }

        protected async Task OpenFilterDialog()
        {
            await Task.Delay(5);
            FilterDialogIsOpen = true;
        }

        protected void CloseDialog() => DialogIsOpen = false;

        protected void CloseFilterDialog() => FilterDialogIsOpen = false;

        protected async Task ConfirmActionIntoDialog()
        {
            DialogIsOpen = false;
            if (!_isEdit)
            {
                var addedFueling = await FuelingService.Create(CarId.Id, DriverId.Id, Fueling);

                if (addedFueling != null)
                {
                    Toaster.Add("Dodano pomyślnie!", MatToastType.Success, "Sukces");
                    Saved = true;
                    if (addedFueling?.Id > 0)
                    {
                        Fueling = addedFueling;
                    }
                }
                else
                {
                    Toaster.Add("Coś poszło nie tak. Spróbuj ponownie!", MatToastType.Danger, "Błąd");
                    Saved = false;
                }
            }
            else
            {
                await FuelingService.Update(CarId.Id, DriverId.Id, Fueling);
                Toaster.Add("Zaaktualizowano pomyślnie!", MatToastType.Success, "Sukces");
                Saved = true;
            }
            StateHasChanged();
        }

        public bool FilterData(FuelingViewModel element)
        {
            var carFilteringResult = false;
            var columnFilteringResult = false;
            var filterByNullOrSpace = string.IsNullOrWhiteSpace(SearchStringToFilterDialog);
            var filterByCar = SearchStringToFilterDialog != null && (element.Car.Brand.Contains(SearchStringToFilterDialog, StringComparison.OrdinalIgnoreCase) ||
                                                                     element.Car.Model.Contains(SearchStringToFilterDialog, StringComparison.OrdinalIgnoreCase));
            var filterByDate = element.Date >= StartFilterDate && element.Date <= EndFilterDate;

            var dateFilteringResult = filterByDate;
            if (dateFilteringResult && filterByNullOrSpace) return true;
            
            switch (ChooseColumnToFilter)
            {
                case "Fuel":
                    carFilteringResult = filterByCar;
                    dateFilteringResult = filterByDate;
                    switch (ChooseOperationToFilter)
                    {
                        case "==":
                            columnFilteringResult = element.Fuel == UpperValue & element.Fuel == LowerValue;
                            break;
                        case ">=":
                            columnFilteringResult = element.Fuel <= UpperValue & element.Fuel >= LowerValue;
                            break;
                        case "<=":
                            columnFilteringResult = element.Fuel >= UpperValue & element.Fuel <= LowerValue;
                            break;
                    }
                    break;

                case "Cost":
                    carFilteringResult = filterByCar;
                    switch (ChooseOperationToFilter)
                    {
                        case "==":
                            columnFilteringResult = element.Cost == UpperValue & element.Cost == LowerValue;
                            break;                                                       
                        case ">=":                                                       
                            columnFilteringResult = element.Cost <= UpperValue & element.Cost >= LowerValue;
                            break;                                                       
                        case "<=":                                                       
                            columnFilteringResult = element.Cost >= UpperValue & element.Cost <= LowerValue;
                            break;
                    }
                    break;

                case "RefuelingMileage":
                    carFilteringResult = filterByCar;
                    switch (ChooseOperationToFilter)
                    {
                        case "==":
                            columnFilteringResult = element.RefuelingMileage == UpperValue & element.RefuelingMileage == LowerValue;
                            break;                                                                   
                        case ">=":                                                                   
                            columnFilteringResult = element.RefuelingMileage <= UpperValue & element.RefuelingMileage >= LowerValue;
                            break;                                                                   
                        case "<=":                                                                   
                            columnFilteringResult = element.RefuelingMileage >= UpperValue & element.RefuelingMileage <= LowerValue;
                            break;
                    }
                    break;
            }
            if (carFilteringResult & columnFilteringResult & dateFilteringResult) return true;

            return false;
        }

        protected async Task<IEnumerable<CarViewModel>> AutocompleteDropdownCars(string searchValue)
        {
            if (string.IsNullOrEmpty(searchValue))
                return Cars;

            return await Task.FromResult(Cars.Where(x => x.Brand.ToLower().Contains(searchValue.ToLower())).ToList());
        }

        protected async Task<IEnumerable<DriverViewModel>> AutocompleteDropdownDrivers(string searchValue)
        {
            if (string.IsNullOrEmpty(searchValue))
                return Drivers;

            return await Task.FromResult(Drivers.Where(x => x.Name.ToLower().Contains(searchValue.ToLower())).ToList());
        }

        protected async Task ConfirmFilterDialog()
        {
            await Task.Delay(5);
            FilterDialogIsOpen = false;
        }
    }
}
