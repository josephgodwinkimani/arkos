﻿using System.Text.Json;
using System.Threading.Tasks;
using Arkos.Workshop.UI.FuelingCrm.Services.Domain;
using Arkos.Workshop.UI.FuelingCrm.ViewModels;
using System.Collections.Generic;
using System.Net.Http;

namespace Arkos.Workshop.UI.FuelingCrm.Services
{
    using System.Text;

    public class FuelingService : IFuelingService
    {
        private readonly HttpClient _httpClient;

        public FuelingService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<FuelingViewModel> Create(int carId, int driverId, FuelingViewModel item)
        {
            string relativeUri = $"Fuelings?carId={carId}&driverId={driverId}";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(relativeUri, bodyJson);

            if (response.IsSuccessStatusCode)
            {
                return await JsonSerializer.DeserializeAsync<FuelingViewModel>(
                    await response.Content.ReadAsStreamAsync(),
                    new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }

            return null;
        }

        public async Task<ICollection<FuelingViewModel>> GetAll(bool includeAll = true)
        {
            string relativeUri = $"?includeAll={includeAll}";
            var uri = new System.Uri(_httpClient.BaseAddress, relativeUri);
            return await JsonSerializer.DeserializeAsync<ICollection<FuelingViewModel>>(
                await _httpClient.GetStreamAsync(uri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public Task<IList<FuelingViewModel>> GetByCarId(int carId)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<FuelingViewModel>> GetByDriverId(int driverId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<FuelingViewModel> GetById(int id)
        {
            string relativeUri = $"Fuelings/{id}"; //TODO
            return await JsonSerializer.DeserializeAsync<FuelingViewModel>(
                await _httpClient.GetStreamAsync(relativeUri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task Update(int carId, int driverId, FuelingViewModel item)
        {
            string relativeUri = $"Fuelings/{item.Id}?carId={carId}&driverId={driverId}";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            await _httpClient.PutAsync(relativeUri, bodyJson);
        }
    }
}
