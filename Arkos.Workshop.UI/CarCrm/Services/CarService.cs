﻿using Arkos.Workshop.UI.CarCrm.ViewModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Arkos.Workshop.UI.CarCrm.Services.Domain;

namespace Arkos.Workshop.UI.CarCrm.Services
{
    public class CarService : ICarService
    {
        private readonly HttpClient _httpClient;

        public CarService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IList<CarViewModel>> GetAll(bool includeAll = true)
        {
            string relativeUri = $"?includeAll={includeAll}";
            var uri = new System.Uri(_httpClient.BaseAddress, relativeUri);
            return await JsonSerializer.DeserializeAsync<IList<CarViewModel>>(
                await _httpClient.GetStreamAsync(uri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task<CarViewModel> GetById(int id)
        {
            string relativeUri = $"Cars/{id}";
            return await JsonSerializer.DeserializeAsync<CarViewModel>(
                await _httpClient.GetStreamAsync(relativeUri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task<CarViewModel> Create(CarViewModel item)
        {
            string relativeUri = $"Cars";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(relativeUri, bodyJson);

            if (response.IsSuccessStatusCode)
            {
                return await JsonSerializer.DeserializeAsync<CarViewModel>(
                    await response.Content.ReadAsStreamAsync(),
                    new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }

            return null;
        }

        public async Task Update(CarViewModel item)
        {
            string relativeUri = $"Cars/{item.Id}";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            await _httpClient.PutAsync(relativeUri, bodyJson);
        }
    }
}
