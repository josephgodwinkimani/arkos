﻿using System.Collections.Generic;
using Arkos.Data;
using System.ComponentModel.DataAnnotations;

namespace Arkos.Workshop.UI.CarCrm.ViewModels
{
    public class CarViewModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Brand { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public string Plate { get; set; }
        [Required]
        public bool IsActive { get; set; } = true;

        public List<Fueling> Fuelings { get; set; }
    }
}
