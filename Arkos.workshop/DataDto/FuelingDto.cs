﻿using System;
namespace Arkos.Workshop.Api.DataDto
{
    public class FuelingDto
    {
        public int Id { get; set; }

        public int CarId { get; set; }

        public int DriverId { get; set; }

        public double Fuel { get; set; }

        public double Cost { get; set; }

        public int RefuelingMileage { get; set; }

        public DateTime? Date { get; set; }
    }
}
