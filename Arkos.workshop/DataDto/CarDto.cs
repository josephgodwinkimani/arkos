﻿namespace Arkos.Workshop.Api.DataDto
{
    public class CarDto
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string Plate { get; set; }

        public bool? IsActive { get; set; }
    }
}
