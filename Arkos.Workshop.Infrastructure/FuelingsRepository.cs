﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Arkos.Workshop.Infrastructure
{
    public class FuelingsRepository : GenericRepository<Fueling>, IFuelingsRepository
    {
        private readonly ArkosWorkshopContext _arkosContext;

        public FuelingsRepository(ArkosWorkshopContext context) : base(context)
        {
            _arkosContext = context;
        }

        public async Task<Fueling> Create(int carId, int driverId, Fueling item)
        {
            item.Car = _arkosContext.Cars.Find(carId);
            item.Driver = _arkosContext.Drivers.Find(driverId);
            var ret = _arkosContext.Fuelings.Add(item);
            await _arkosContext.SaveChangesAsync();
            return ret.Entity;
        }

        public async Task<Fueling> FindById(int fuelingId, bool includeAll = true)
        {
            if (includeAll)
            {
                return
                    await _arkosContext
                        .Fuelings
                        .Include(q => q.Car)
                        .Include(q =>q.Driver)
                        .SingleOrDefaultAsync(q => q.Id == fuelingId);
            }
            else
            {
                return await base.FindAsync(q => q.Id == fuelingId);
            }
        }


        public async Task<Fueling> Update(int id, int carId, int driverId, Fueling item)
        {
            item.Car = _arkosContext.Cars.Find(carId);
            item.Driver = _arkosContext.Drivers.Find(driverId);
            return await base.UpdateAsync(id, item);
        }

        public async Task<IList<Fueling>> FindByCarId(int carId)
        {
            return
                    await _arkosContext
                        .Fuelings
                        .Where(q => q.Car.Id == carId)
                        .ToListAsync();
        }

        public async Task<IList<Fueling>> FindByDriverId(int driverId)
        {
            return
                    await _arkosContext
                        .Fuelings
                        .Where(q => q.Driver.Id == driverId)
                        .ToListAsync();
        }


        public async Task<ICollection<Fueling>> GetAll(bool includeAll)
        {
            if (includeAll)
            {
                return
                    await _arkosContext
                        .Fuelings
                        .Include(q => q.Car)
                        .Include(q => q.Driver)
                        .ToListAsync();
            }
            else
            {
                return
                    await _arkosContext
                        .Fuelings
                        .ToListAsync();
            }
        }

        public async Task<Fueling> Delete(int id)
        {
            var del = await FindById(id);
            if (del != null)
            {
                _arkosContext.Remove(del);
                _arkosContext.SaveChanges();
            }

            return del;
        }
    }
}
